FROM php:7.2-apache
WORKDIR /var/www/html
EXPOSE 80 443
CMD [ "sh", "-c", "/usr/local/bin/build.sh; /usr/local/bin/apache2-foreground; cron -f" ]

